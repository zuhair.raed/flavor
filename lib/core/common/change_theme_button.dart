import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../application.dart';

class ChangeThemeModeButton extends ConsumerWidget {
  const ChangeThemeModeButton({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final theme = ref.watch(themeProvider);
    final color = Theme.of(context).colorScheme;

    return TextButton.icon(
      onPressed: () {
        application.setTheme(ref);
      },
      icon: Icon(
        theme == ThemeMode.light ? Icons.dark_mode : Icons.light_mode,
        color: color.secondary,
      ),
      label: Text(
        theme == ThemeMode.light ? 'Dark Mode' : 'Light Mode',
        style: TextStyle(color: color.secondary),
      ),
    );
  }
}
