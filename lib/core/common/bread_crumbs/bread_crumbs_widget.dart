import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../../style/style.dart';
import 'bread_crumbs_circle.dart';

class BreadCrumbsWidget extends StatelessWidget {
  const BreadCrumbsWidget({super.key, required this.stepCount});
  final int stepCount;

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, rer, _) {
        final crumbIndex = rer.watch(breadCrumbsIndexProvider);
        final color = Theme.of(context).colorScheme;
        final List<Widget> breadCrumbs = List.generate(
          stepCount,
          (index) {
            return Row(
              children: [
                BreadCrumbsCircle(index: index),
                if (index != stepCount - 1)
                  SizedBox(
                    width: 50,
                    child: Divider(
                      color: (crumbIndex > index + 1)
                          ? Style.primary
                          : color.scrim,
                    ),
                  ),
              ],
            );
          },
        );

        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: breadCrumbs,
        );
      },
    );
  }
}

final breadCrumbsIndexProvider = StateProvider<int>((ref) {
  return 1;
});
