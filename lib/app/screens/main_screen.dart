import 'package:auto_route/auto_route.dart';
import 'package:flavor/core/common/main_button.dart';
import 'package:flavor/core/l10n/l10n.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '/core/application.dart';

import '../../core/style/style.dart';

@RoutePage()
class MainScreen extends ConsumerWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context, ref) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('General App'),
      ),
      body: Column(
        children: [
          DropdownButton(
              value: ref.watch(langProvider),
              items: L10n.supportedLocales.map((local) {
                return DropdownMenuItem(
                  value: local,
                  child: Row(
                    children: [
                      Text(local.languageCode),
                      const SizedBox(width: 8),
                      Text(L10n.getFlag(local.languageCode)),
                    ],
                  ),
                  onTap: () => application.setLanguage(local, ref),
                );
              }).toList(),
              onChanged: (_) {}),
          Center(
            child: Text(
              application.translate.title,
            ),
          ),
          Center(
            child: Text(
              application.translate.message,
            ),
          ),
          Center(
            child: Text(
              application.translate.hola('Ard Labs'),
            ),
          ),
          Style.hight64,
          MainButton(
              text: 'Change Theme',
              onPressed: () {
                application.setTheme(ref);
              }),
        ],
      ),
    );
  }
}
