import 'package:flutter/material.dart';

class CommonShapes {
  // THIS IS FOR CONTAINER COMMON RADIUS
  static const BorderRadiusGeometry containerRadius20 =
      BorderRadius.all(Radius.circular(20));
  static const BorderRadiusGeometry containerRadius15 =
      BorderRadius.all(Radius.circular(15));
  static const BorderRadiusGeometry containerRadius10 =
      BorderRadius.all(Radius.circular(10));
  static const BorderRadiusGeometry containerRadius5 =
      BorderRadius.all(Radius.circular(5));

  static BorderRadiusGeometry customRadiusForContainer(double radius) {
    return BorderRadius.all(Radius.circular(radius));
  }

  // THIS IS FOR CARD COMMON RADIUS
  static const OutlinedBorder cardRadius15 = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(15)),);
  static const OutlinedBorder cardRadius10 = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(10)),);
  static const OutlinedBorder cardRadius5 = RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(5)),);

  static OutlinedBorder customRadiusForCard(double radius) {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(radius)),);
  }

// THIS METHOD WILL USED IF WE HAVE CUSTOM SHAPE ON DESIGN
  static OutlinedBorder roundedOnly({
    double topLeft = 0,
    double topRight = 0,
    double bottomLeft = 0,
    double bottomRight = 0,
  }) {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(topLeft),
            topRight: Radius.circular(topRight),
            bottomLeft: Radius.circular(bottomLeft),
            bottomRight: Radius.circular(bottomRight),),);
  }
}
