import 'package:auto_route/auto_route.dart';
import 'package:flavor/app/routes/app_router.gr.dart';
import 'package:flavor/core/application.dart';
import 'package:flavor/core/common/main_button.dart';
import 'package:flavor/core/utils/flavor.dart';
import 'package:flutter/material.dart';

@RoutePage()
class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final flavor = Flavor.flavor;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const CircularProgressIndicator(),
            MainButton(
                text: 'Go to next screen',
                onPressed: () {
                  application.appRouter.push(const MainRoute());
                }),
            Text('Flavor: ${flavor.title}'),
          ],
        ),
      ),
    );
  }
}
