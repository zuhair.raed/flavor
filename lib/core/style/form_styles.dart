import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'style.dart';

class FormStyles {
  static textDirection(String text) {
    return intl.Bidi.detectRtlDirectionality(text)
        ? TextDirection.rtl
        : TextDirection.ltr;
  }

  static InputDecoration formStyle(
    String hint, {
    Widget? suffixIcon,
    Widget? prefixIcon,
    bool? showMaxLength = true,
  }) {
    const borderColor = Style.primary;
    const hintColor = Style.secondary;
    const borderRadius = 5.0;
    return InputDecoration(
      isDense: true,
      // isCollapsed: true,
      contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 8),
      labelStyle: Style.mainFont.titleSmall?.copyWith(),
      enabledBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: borderColor),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      ),
      focusedBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: borderColor),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      ),
      border: const OutlineInputBorder(
        borderSide: BorderSide(color: borderColor),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      ),
      errorBorder: const OutlineInputBorder(
        borderSide: BorderSide(color: Style.error, width: 1),
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
      ),
      errorStyle: Style.mainFont.bodySmall?.copyWith(color: Style.error),
      errorMaxLines: 2,
      // helperText: ' ',
      // fillColor: Style.field,
      suffixIcon: suffixIcon,
      counterText: showMaxLength == true ? null : '',
      prefixIcon: prefixIcon == null
          ? null
          : Padding(
              padding: const EdgeInsetsDirectional.only(end: 8),
              child: prefixIcon,
            ),
      // filled: true,
      hintStyle: Style.mainFont.bodySmall?.copyWith(color: hintColor),
      hintText: hint,
    );
  }

  // static InputDecoration searchField(String hint) {
  //   return InputDecoration(
  //     isDense: true,
  //     labelStyle: Style.mainFont.bodySmall,
  //     enabledBorder: const OutlineInputBorder(
  //       borderSide: BorderSide(
  //         width: 0,
  //         style: BorderStyle.none,
  //       ),
  //       borderRadius: BorderRadius.all(Radius.circular(10)),
  //     ),
  //     focusedBorder: const OutlineInputBorder(
  //       borderSide: BorderSide(
  //         width: 0,
  //         style: BorderStyle.none,
  //       ),
  //       borderRadius: BorderRadius.all(Radius.circular(10)),
  //     ),
  //     border: const OutlineInputBorder(
  //       borderSide: BorderSide(
  //         width: 0,
  //         style: BorderStyle.none,
  //       ),
  //       borderRadius: BorderRadius.all(Radius.circular(10)),
  //     ),
  //     errorBorder: const OutlineInputBorder(
  //       borderSide: BorderSide(
  //         width: 0,
  //         style: BorderStyle.none,
  //       ),
  //       borderRadius: BorderRadius.all(Radius.circular(10)),
  //     ),
  //     errorStyle: Style.mainFont.bodySmall?.copyWith(color: Style.red),
  //     fillColor: Style.field,
  //     filled: true,
  //     hintStyle: Style.mainFont.bodyMedium?.copyWith(color: Style.darkText),
  //     hintText: hint,
  //     contentPadding: const EdgeInsets.symmetric(vertical: 0, horizontal: 20),
  //     prefixIcon: Padding(
  //       padding: const EdgeInsets.all(12),
  //       child: SvgPicture.asset(
  //         SvgAssets.search,
  //         colorFilter: const ColorFilter.mode(
  //           Style.darkText,
  //           BlendMode.srcIn,
  //         ),
  //       ),
  //     ),
  //     prefixIconColor: Style.background,
  //   );
  // }

  static InputBorder withoutBorder({double? radius}) {
    return OutlineInputBorder(
      borderSide: const BorderSide(
        width: 0,
        style: BorderStyle.none,
      ),
      gapPadding: 0,
      borderRadius: BorderRadius.all(
        Radius.circular(radius ?? 20),
      ),
    );
  }
}
