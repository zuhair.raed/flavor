import 'package:flavor/core/manager/app_settings_manager.dart';
import 'package:flavor/core/utils/flavor.dart';
import 'package:flavor/my_app.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppSettingsManager().initState();
}

Future<void> main() async {
  // initilaize the app Flavor 

  Flavor.flavor = GeneralFlavor();

  await initApp();
  runApp(const ProviderScope(
    child: MyApp(),
  ));
}
