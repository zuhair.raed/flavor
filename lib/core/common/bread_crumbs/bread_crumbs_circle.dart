import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '../../style/style.dart';

import 'bread_crumbs_widget.dart';

class BreadCrumbsCircle extends ConsumerWidget {
  final int index;
  const BreadCrumbsCircle({
    super.key,
    required this.index,
  });

  @override
  Widget build(BuildContext context, ref) {
    final crumbIndex = ref.watch(breadCrumbsIndexProvider);
    final color = Theme.of(context).colorScheme;
    return Container(
      height: 20,
      width: 20,
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: (crumbIndex > index) ? Style.primary : color.scrim,
          width: 1,
        ),
      ),
      child: Container(
        height: 10,
        width: 10,
        decoration: BoxDecoration(
          color: (crumbIndex > index) ? Style.primary : color.scrim,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Center(
          child: Text(
            (index + 1).toString(),
            style: const TextStyle(
              color: Colors.white,
              fontSize: 10,
            ),
          ),
        ),
      ),
    );
  }
}
