import 'package:flavor/core/style/form_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import '../../style/style.dart';
import '../text_field_title.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class GeneralFileld extends HookConsumerWidget {
  final String? title;
  final Function(String)? onChanged;
  final TextInputType? keyboardType;
  final String? hintText;
  final String? Function(String?)? validator;

  const GeneralFileld({
    super.key,
    this.title,
    this.onChanged,
    this.hintText,
    this.validator,
    this.keyboardType,
  });
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final controller = useTextEditingController();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (title != null) TextFieldTitle(title: title!),
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: TextFormField(
            controller: controller,
            keyboardType: keyboardType,
            style: Style.mainFont.bodyMedium,
            decoration: FormStyles.formStyle(
              hintText ?? title ?? '',
            ),
            validator: validator,
            onChanged: (v) {
              controller.text = v;
              onChanged?.call(v);
            },
          ),
        ),
      ],
    );
  }
}
