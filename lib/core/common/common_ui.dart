import 'package:app_settings/app_settings.dart';
import 'package:flavor/core/common/loading_spinner.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../utils/utils.dart';
import '../style/style.dart';
import '../../../core/application.dart';
import 'dialogs.dart';

mixin CommonUi {
  /// THIS METHOD USED TO CHECK IF THE INTERNET IS WORKING OR NOT, AND IF NOT THE PHONE SETTING WILL OPEN
  static bool checkInternetShown = false;

  static Future<void> checkInternet(BuildContext context) async {
    final isOnline = await Utils.isOnline();
    if (!isOnline && !checkInternetShown && context.mounted) {
      checkInternetShown = true;
      messageDialog(
        context,
        title: application.translate.noInternet,
        message: application.translate.noInternetMsg,
        onDismiss: () {
          checkInternetShown = false;
        },
        onConfirm: () {
          AppSettings.openAppSettings(type: AppSettingsType.wifi);
        },
      );
    }
  }
//

  /// THIS METHOD USED TO SHOW DIALOGS BASED ON OS PLATFORM
  static Future<void> messageDialog<T>(
    BuildContext context, {
    String title = '',
    String message = '',
    Function? onConfirm,
    Function? onDismiss,
    bool? withSecondButton = true,
    String? firstButtonText,
    String? secondButtonText,
    ActionStyle firstActionStyle = ActionStyle.normal,
    ActionStyle secondActionStyle = ActionStyle.normal,
  }) async {
    final _ = await Dialogs.showOSDialog<T>(
      context: context,
      title: title,
      message: message,
      firstActionStyle: firstActionStyle,
      secondActionStyle: secondActionStyle,
      firstButtonText:
          firstButtonText ?? MaterialLocalizations.of(context).okButtonLabel,
      firstCallBack: () async {
        application.postDelayed(
          callbak: () {
            onConfirm?.call();
          },
        );
      },
      secondButtonText: withSecondButton == true
          ? secondButtonText ??
              MaterialLocalizations.of(context).cancelButtonLabel
          : null,
    );
    if (onDismiss != null) {
      onDismiss();
    }
  }

  static snackBar(
    BuildContext context,
    String message, {
    Color? backgroundColor,
    Color? textColor,
    SnackBarAction? action,
    int? durationMilliSeconds,
    bool? showCloseIcon = true,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          message,
          style: Style.mainFont.labelMedium
              ?.copyWith(color: textColor ?? Colors.white),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        behavior: SnackBarBehavior.floating,
        backgroundColor: backgroundColor ?? Style.secondary,
        showCloseIcon: showCloseIcon,
        closeIconColor: Style.secondary,
        duration: Duration(milliseconds: durationMilliSeconds ?? 3000),
        action: action,
      ),
    );
  }

  static simpleToast({
    required String message,
    Color? backgroundColor,
    ToastGravity? gravity,
  }) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: gravity ?? ToastGravity.BOTTOM,
      backgroundColor: backgroundColor ?? Colors.black,
      textColor: Colors.white,
      fontSize: 12,
    );
  }

  static void hideKeyboard(BuildContext context) {
    // FocusScope.of(context).requestFocus(FocusNode());
    // FocusManager.instance.primaryFocus?.unfocus();
    SystemChannels.textInput.invokeMethod('TextInput.hide');
  }

  static void loadingDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        final color = Theme.of(context).colorScheme;
        return Center(
          child: SizedBox(
            width: 90,
            child: AlertDialog(
              backgroundColor: color.surface,
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              elevation: 0,
              insetPadding: EdgeInsets.zero,
              content: const LoadingSpinner(
                height: 45,
                width: 45,
              ),
              // LottieBuilder.asset(
              // height: 45,
              // width: 45,
              //   AnimeAssets.loading,
              // ),
            ),
          ),
        );
      },
    );
  }
}
