import 'package:flutter/material.dart';

import '../style/style.dart';
import 'app_padding.dart';

class EmptyResult extends StatelessWidget {
  final String message;
  final String? svgAsset;
  const EmptyResult({
    super.key,
    required this.message,
    this.svgAsset,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AppPadding(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 20),

            // LottieBuilder.asset(AnimeAssets.empty),
            const SizedBox(height: 20),
            Text(
              message,
              textAlign: TextAlign.center,
              style: Style.mainFont.bodyMedium,
            ),
          ],
        ),
      ),
    );
  }
}
