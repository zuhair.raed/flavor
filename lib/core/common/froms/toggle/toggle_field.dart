import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import '/core/style/style.dart';
import '../../text_field_title.dart';
import 'toggle_switch/toggle_switch.dart';

class ToggleField extends ConsumerWidget {
  final List<String> list;
  final String? title;
  final List<IconData?>? icons;
  final Function(int?)? onChanged;
  const ToggleField({
    super.key,
    required this.list,
    this.onChanged,
    this.icons,
    this.title,
  });

  @override
  Widget build(BuildContext context, ref) {
    final color = Theme.of(context).colorScheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: title != null,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: TextFieldTitle(title: title ?? ''),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: ToggleSwitch(
            minWidth: double.infinity,
            minHeight: 40,
            initialLabelIndex: 1,
            cornerRadius: 5,
            activeFgColor: Colors.white,
            inactiveBgColor: Colors.transparent,
            inactiveFgColor: Style.textBody,
            borderWidth: 1,
            borderColor: [color.scrim],
            activeBgColor: const [Style.secondary],
            animate: true,
            animationDuration: 300,
            totalSwitches: list.length,
            labels: list,
            icons: icons,
            onToggle: (c) {
              onChanged?.call(c);
            },
          ),
        ),
      ],
    );
  }
}
