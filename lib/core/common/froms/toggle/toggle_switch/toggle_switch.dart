import 'package:flutter/material.dart';
import 'dart:math';

import 'src/custom_border_paint.dart';
import 'src/row_to_column.dart';
import 'src/toggle_utils.dart';

typedef OnToggle = void Function(int? index);
typedef CancelToggle = Future<bool> Function(int? index);

// ignore: must_be_immutable
class ToggleSwitch extends StatefulWidget {
  final List<Color>? borderColor;

  final Color dividerColor;

  final List<Color>? activeBgColor;

  final Color? activeFgColor;

  final Color? inactiveBgColor;

  final Color? inactiveFgColor;

  final List<String>? labels;

  final List<bool>? states;

  final int? totalSwitches;

  final List<IconData?>? icons;

  final List<List<Color>?>? activeBgColors;

  final List<TextStyle?>? customTextStyles;

  final List<Icon?>? customIcons;

  final List<double>? customWidths;

  final List<double>? customHeights;

  final double minWidth;

  final double minHeight;

  final double cornerRadius;

  final double fontSize;

  final double iconSize;

  final double? dividerMargin;

  final double? borderWidth;

  final OnToggle? onToggle;

  final CancelToggle? cancelToggle;

  final bool changeOnTap;

  final bool animate;

  final int animationDuration;

  final bool radiusStyle;

  final bool textDirectionRTL;

  final Curve curve;

  int? initialLabelIndex;

  final bool doubleTapDisable;

  final bool isVertical;

  final List<Border?>? activeBorders;

  final bool centerText;

  final bool multiLineText;

  final List<Widget>? customWidgets;
  ToggleSwitch({
    super.key,
    this.totalSwitches,
    this.labels,
    this.states,
    this.borderColor,
    this.borderWidth,
    this.dividerColor = Colors.white30,
    this.activeBgColor,
    this.activeFgColor,
    this.inactiveBgColor,
    this.inactiveFgColor,
    this.onToggle,
    this.cancelToggle,
    this.cornerRadius = 8.0,
    this.initialLabelIndex = 0,
    this.minWidth = 72.0,
    this.minHeight = 40.0,
    this.changeOnTap = true,
    this.icons,
    this.activeBgColors,
    this.customTextStyles,
    this.customIcons,
    this.customWidths,
    this.customHeights,
    this.animate = false,
    this.animationDuration = 800,
    this.curve = Curves.easeIn,
    this.radiusStyle = false,
    this.textDirectionRTL = false,
    this.fontSize = 14.0,
    this.iconSize = 17.0,
    this.dividerMargin = 8.0,
    this.doubleTapDisable = false,
    this.isVertical = false,
    this.activeBorders,
    this.centerText = false,
    this.multiLineText = false,
    this.customWidgets,
  });

  @override
  State<ToggleSwitch> createState() => _ToggleSwitchState();
}

class _ToggleSwitchState extends State<ToggleSwitch>
    with AutomaticKeepAliveClientMixin<ToggleSwitch> {
  late List<Color> _borderColor;

  late double _borderWidth;

  late double _cornerRadius;

  late int _totalSwitches;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    _totalSwitches = widget.totalSwitches ??
        [
          widget.labels?.length ?? 0,
          widget.icons?.length ?? 0,
          widget.customIcons?.length ?? 0,
          widget.customWidgets?.length ?? 0,
        ].reduce(max);

    _borderColor = widget.borderColor ?? [Colors.transparent];

    if (_borderColor.length == 1) {
      _borderColor = List<Color>.filled(2, _borderColor[0]);
    }

    _borderWidth =
        widget.borderWidth ?? (widget.borderColor == null ? 0.0 : 3.0);

    _cornerRadius = widget.cornerRadius - _borderWidth;
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final List<bool> states =
        widget.states ?? List<bool>.filled(_totalSwitches, true);

    final inactiveBgColor =
        widget.inactiveBgColor ?? Theme.of(context).disabledColor;

    return CustomPaint(
      painter: CustomBorderPaint(
        thickness: _borderWidth,
        radius: Radius.circular(widget.cornerRadius),
        gradient: LinearGradient(
          colors: _borderColor,
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: Container(
        margin: EdgeInsets.all(_borderWidth),
        decoration: BoxDecoration(
          color: inactiveBgColor,
          borderRadius: BorderRadius.circular(_cornerRadius),
        ),
        height: !widget.isVertical ? widget.minHeight + _borderWidth : null,
        width: widget.isVertical ? widget.minWidth + _borderWidth : null,
        child: RowToColumn(
          isVertical: widget.isVertical,
          mainAxisSize: MainAxisSize.min,
          children: List.generate(_totalSwitches * 2 - 1, (index) {
            final active =
                index ~/ 2 == widget.initialLabelIndex && states[index ~/ 2];

            if (index % 2 == 1) {
              return _divider(index: index, active: active);
            } else {
              return _switchItem(
                index: index,
                active: active,
                states: states,
              );
            }
          }),
        ),
      ),
    );
  }

  Container _divider({required int index, required bool active}) {
    final activeDivider = active ||
        (widget.initialLabelIndex != null &&
            index ~/ 2 == widget.initialLabelIndex! - 1);

    return Container(
      width: !widget.isVertical ? 1 : double.infinity,
      height: widget.isVertical ? 1 : double.infinity,
      color: activeDivider ? Colors.transparent : widget.dividerColor,
      margin: widget.isVertical
          ? EdgeInsets.symmetric(
              horizontal: activeDivider ? 0.0 : widget.dividerMargin!,
            )
          : EdgeInsets.symmetric(
              vertical: activeDivider ? 0.0 : widget.dividerMargin!,
            ),
    );
  }

  Widget _switchItem({
    required int index,
    required bool active,
    required List<bool> states,
  }) {
    final inactiveFgColor =
        widget.inactiveFgColor ?? Theme.of(context).textTheme.bodyLarge!.color;

    final activeFgColor = widget.activeFgColor ??
        Theme.of(context).primaryTextTheme.bodyLarge!.color;

    final activeBgColor =
        widget.activeBgColor ?? [Theme.of(context).primaryColor];

    final fgColor = active ? activeFgColor : inactiveFgColor;

    List<Color> bgColor = [Colors.transparent];

    if (active) {
      bgColor = widget.activeBgColors == null
          ? activeBgColor
          : (widget.activeBgColors![index ~/ 2] ?? activeBgColor);
    }

    if (bgColor.length == 1) {
      bgColor = List<Color>.filled(2, bgColor[0]);
    }

    double height = ToggleUtils.calculateHeight(
      context: context,
      index: index ~/ 2,
      totalSwitches: _totalSwitches,
      customHeights: widget.customHeights,
      minHeight: widget.minHeight,
    );

    double width = ToggleUtils.calculateWidth(
      context: context,
      index: index ~/ 2,
      totalSwitches: _totalSwitches,
      customWidths: widget.customWidths,
      minWidth: widget.minWidth,
    );

    Border? activeBorder;
    if (widget.activeBorders != null) {
      activeBorder = widget.activeBorders!.length == 1
          ? widget.activeBorders![0]
          : (widget.activeBorders!.length > index ~/ 2 &&
                  widget.activeBorders![index ~/ 2] != null
              ? widget.activeBorders![index ~/ 2]!
              : null);
    }

    Widget icon = _icon(
      index: index ~/ 2,
      height: height,
      width: width,
      fgColor: fgColor,
    );

    return Flexible(
      child: GestureDetector(
        onTap: () => states[index ~/ 2] ? _handleOnTap(index ~/ 2) : null,
        child: AnimatedContainer(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          constraints: BoxConstraints(
            maxWidth:
                widget.isVertical ? const BoxConstraints().maxWidth : width,
            maxHeight:
                widget.isVertical ? height : const BoxConstraints().maxHeight,
          ),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: active ? activeBorder : null,
            borderRadius: widget.radiusStyle
                ? BorderRadius.all(Radius.circular(_cornerRadius))
                : _borderRadius(index: index),
            gradient: LinearGradient(
              colors: bgColor,
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
          duration: Duration(
            milliseconds: widget.animate ? widget.animationDuration : 0,
          ),
          curve: widget.curve,
          child: widget.customWidgets != null
              ? widget.customWidgets![index ~/ 2]
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    icon,
                    Flexible(
                      child: Container(
                        padding: EdgeInsets.only(
                          left: (icon is SizedBox) ? 0.0 : 5.0,
                        ),
                        child: Text(
                          widget.labels?[index ~/ 2] ?? '',
                          textAlign:
                              (widget.centerText) ? TextAlign.center : null,
                          style:
                              _textStyle(index: index ~/ 2, fgColor: fgColor),
                          overflow: (!widget.multiLineText)
                              ? TextOverflow.ellipsis
                              : null,
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }

  void _handleOnTap(int index) async {
    int? newIndex = index;
    if (widget.doubleTapDisable && widget.initialLabelIndex == index) {
      newIndex = null;
    }

    final cancel = await widget.cancelToggle?.call(newIndex) ?? false;
    if (cancel) {
      return;
    }

    if (widget.changeOnTap) {
      setState(() => widget.initialLabelIndex = newIndex);
    }

    widget.onToggle?.call(newIndex);
  }

  Widget _icon({
    required int index,
    required double height,
    required double width,
    Color? fgColor,
  }) {
    Widget icon = widget.icons != null && widget.icons![index] != null
        ? Icon(
            widget.icons![index],
            color: fgColor,
            size: widget.isVertical
                ? widget.iconSize > height / 3
                    ? height / 3
                    : widget.iconSize
                : widget.iconSize > width / 3
                    ? width / 3
                    : widget.iconSize,
          )
        : const SizedBox();

    if (widget.customIcons != null && widget.customIcons![index] != null) {
      icon = widget.customIcons![index]!;
    }

    return icon;
  }

  TextStyle _textStyle({required int index, Color? fgColor}) {
    TextStyle defaultTextStyle = TextStyle(
      color: fgColor,
      fontSize: widget.fontSize,
    );

    TextStyle oneIndexStyle() {
      if (widget.customTextStyles![0]!.color == null) {
        return widget.customTextStyles![0]!.copyWith(
          color: fgColor,
        );
      }
      return widget.customTextStyles![0]!;
    }

    TextStyle multiIndexStyle() {
      if (widget.customTextStyles![index]!.color == null) {
        return widget.customTextStyles![index]!.copyWith(
          color: fgColor,
        );
      }

      return widget.customTextStyles![index]!;
    }

    TextStyle textStyle = defaultTextStyle;
    if (widget.customTextStyles != null) {
      textStyle = widget.customTextStyles!.length == 1
          ? oneIndexStyle()
          : (widget.customTextStyles!.length > index &&
                  widget.customTextStyles![index] != null
              ? multiIndexStyle()
              : defaultTextStyle);
    }

    return textStyle;
  }

  BorderRadius? _borderRadius({required int index}) {
    BorderRadius? cornerRadius;
    if (index == 0 && !widget.isVertical) {
      cornerRadius = widget.textDirectionRTL
          ? BorderRadius.horizontal(
              right: Radius.circular(_cornerRadius),
            )
          : BorderRadius.horizontal(
              left: Radius.circular(_cornerRadius),
            );
    }
    if (index == 0 && widget.isVertical) {
      cornerRadius = BorderRadius.vertical(
        top: Radius.circular(_cornerRadius),
      );
    }
    if (index ~/ 2 == _totalSwitches - 1 && !widget.isVertical) {
      cornerRadius = widget.textDirectionRTL
          ? BorderRadius.horizontal(
              left: Radius.circular(_cornerRadius),
            )
          : BorderRadius.horizontal(
              right: Radius.circular(_cornerRadius),
            );
    }
    if (index ~/ 2 == _totalSwitches - 1 && widget.isVertical) {
      cornerRadius = BorderRadius.vertical(
        bottom: Radius.circular(_cornerRadius),
      );
    }

    return cornerRadius;
  }
}
