import 'package:logger/logger.dart';

import 'string_utils.dart';

class Log {
  static final logger = Logger(
    // level: Level.warning,
    printer: PrettyPrinter(
      methodCount: 3, // number of method calls to be displayed
      errorMethodCount: 8, // number of method calls if stacktrace is provided
      lineLength: 300, // width of the output
      colors: true, // Colorful log messages
      printEmojis: true, // Print an emoji for each log message
      printTime: true, // Should each log print contain a timestamp
    ),
  );
  static void pr(Object? obj) {
    logger.i(obj);
  }

  static void e(Object? obj) {
    logger.e(obj);
  }

  static void wtf(Object? obj) {
    logger.f(obj);
  }

  static void json(Object? obj) {
    logger.e(StringUtils.prettyJson(obj));
  }

  static void d(Object? t) {
    logger.d(t);
  }
}
