import 'package:flavor/core/utils/flavor.dart';
import 'package:flavor/main.dart';
import 'package:flavor/my_app.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

void main() async {
  Flavor.flavor = HuwaeiFlavor();
  await initApp();

  runApp(const ProviderScope(child: MyApp()));
}
