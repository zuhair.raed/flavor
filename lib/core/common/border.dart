import 'package:flutter/material.dart';

class Red extends StatelessWidget {
  final Widget child;
  final Color? color;
  const Red({
    super.key,
    required this.child,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: color ?? Colors.red, width: 1),),
      child: child,
    );
  }
}
