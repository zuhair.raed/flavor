import 'package:flutter/material.dart';
import '../style/style.dart';

import '../../../../core/application.dart';

class TextFieldTitle extends StatelessWidget {
  final String title;
  final bool isRequired;
  final Color? color;
  const TextFieldTitle({
    super.key,
    required this.title,
    this.isRequired = false,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    // final colorScheme = Theme.of(context).colorScheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // RichText(
        //   text: TextSpan(
        //     text: title,
        //     style: Style.mainFont.titleMedium?.copyWith(color: color.onPrimary),
        //     children: [
        //       TextSpan(
        //         text: isRequired ? '  *' : '',
        //         style: Style.mainFont.titleMedium
        //             ?.copyWith(color: color.onPrimary),
        //       ),
        //     ],
        //   ),
        // ),
        Text(
          title,
          style: Style.mainFont.titleSmall
              ?.copyWith(color: color ?? Style.mainFont.titleSmall?.color),
        ),
        application.isLanguageLTR()
            ? const SizedBox(height: 4)
            : const SizedBox(height: 6),
      ],
    );
  }
}
