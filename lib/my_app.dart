import 'package:flavor/core/l10n/l10n.dart';
import 'package:flavor/core/application.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import 'core/style/style.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, _) {
      final local = ref.watch(langProvider);
      final theme = ref.watch(themeProvider);
      final appRouter = ref.watch(appRouterProvider);
      application.appRouter = appRouter;

      return MaterialApp.router(
        routerConfig: appRouter.config(),
        builder: (context, child) => child!,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        locale: local,
        supportedLocales: L10n.supportedLocales,
        themeMode: theme,
        darkTheme: Style.darkTheme(context),
        theme: Style.appTheme(context),
      );
    });
  }
}

