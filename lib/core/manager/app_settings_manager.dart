import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'shared_preferences_manager.dart';

class AppSettingsManager {
  Future<AppSettingsManager> initState() async {
    await SharedPreferencesManager().initState();
    await getAppLanguage();
    await getAppTheme();
    return this;
  }

  late Locale appLang;
  late ThemeMode appTheme;

  static final AppSettingsManager _instance = AppSettingsManager._internal();
  factory AppSettingsManager() => _instance;
  AppSettingsManager._internal() {
    initState();
  }

  Future<String> getAppLanguage() async {
    final lang = await SharedPreferencesManager().getString('AppLang') ?? 'en';
    AppSettingsManager().appLang = Locale(lang);
    appLang = Locale(lang);
    Intl.defaultLocale = lang;
    return lang;
  }

  Future<String> getAppTheme() async {
    String? theme =
        await SharedPreferencesManager().getString('AppTheme') ?? ' ';
    if (theme == 'dark') {
      appTheme = ThemeMode.dark;
    } else if (theme == 'light') {
      appTheme = ThemeMode.light;
    } else {
      appTheme = ThemeMode.light;
    }
    return theme;
  }

// ============== Set Functions ============== //

  void setAppLanguage(String lang) {
    appLang = Locale(lang);
    SharedPreferencesManager().setString('AppLang', lang);
    Intl.defaultLocale = lang;
  }

  void setAppTheme(ThemeMode theme) {
    appTheme = theme;
    if (theme == ThemeMode.dark) {
      SharedPreferencesManager().setString('AppTheme', 'dark');
    } else {
      SharedPreferencesManager().setString('AppTheme', 'light');
    }
  }

  Future<void> clearLocalDataBase() async {
    SharedPreferencesManager().prefs.clear();
  }
}
