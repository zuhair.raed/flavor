class GeneralFlavor extends Flavor {
  @override
  String title = 'This is General app';
}

class HuwaeiFlavor extends Flavor {
  @override
  String title = 'This is Huwaei app';
}

abstract class Flavor {
  static late Flavor flavor;

  abstract String title;
}
