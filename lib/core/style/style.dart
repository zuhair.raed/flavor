import 'package:flavor/core/application.dart';
import 'package:flavor/core/manager/app_settings_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class Style {
  static const Color primary = Color(0xffF75191); //primary
  static const Color secondary = Colors.white; // onprimary
  static const Color surfaceContainer = Color(0xff303033);
  static const Color surface = Color(0xff212A32); // background
  static const Color error = Colors.red;
  static const Color lightBackground = Colors.white; // background
  static const Color darkBackground = Color(0xff252c37); // background
  static const Color textBody = Color(0xff757575);
  static const Color boldText = Color(0xff545454);
  // static const Color textBody = Color(0xff465364); //
  static const Color darkTextBody = Color(0xffb0b9c6); //
  static const Color darkTextTitle = Color(0xffced5de); //
  static const Color lines = Color(0xffEBEBEB);
  static const Color forGridOnly = Color(0xffE1E1E1);

  static late TextTheme mainFont;
  static late TextTheme secondaryFont;
  static late ThemeData mainTheme;

  static void _setTextTheme(BuildContext context) {
    final isDark = AppSettingsManager().appTheme == ThemeMode.dark;
    final fontColor = isDark ? darkTextBody : textBody;
    final textTitle = isDark ? darkTextTitle : darkBackground;

    final arabicFont = GoogleFonts.almaraiTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: fontColor, displayColor: fontColor);

    mainFont = GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: fontColor, displayColor: fontColor);
    secondaryFont = GoogleFonts.latoTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: fontColor, displayColor: fontColor);

    mainFont = (application.isLanguageLTR() ? mainFont : arabicFont);
    mainFont = mainFont.copyWith(
      titleSmall: mainFont.titleSmall?.copyWith(color: textTitle),
      titleMedium: mainFont.titleMedium?.copyWith(color: textTitle),
      titleLarge: mainFont.titleLarge?.copyWith(color: textTitle),
    );
  }

  /// Controll the app bar theme from here
  static final AppBarTheme appBarTheme = AppBarTheme(
    elevation: 0.0,
    // centerTitle: true,
    systemOverlayStyle: const SystemUiOverlayStyle(
      statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
      statusBarBrightness: Brightness.light, // For iOS (dark icons)
    ),
    actionsIconTheme: const IconThemeData(color: primary),
    titleTextStyle: mainFont.titleMedium?.copyWith(color: secondary),
    color: Style.primary,
    iconTheme: const IconThemeData(color: secondary),
    surfaceTintColor: secondary,
  );

  static final textSelectionThemeData = TextSelectionThemeData(
    selectionColor: primary.withOpacity(0.5),
  );

  static const bottomNavigationBarTheme = BottomNavigationBarThemeData(
    selectedItemColor: secondary,
    unselectedItemColor: secondary,
    showSelectedLabels: false,
    showUnselectedLabels: false,
    selectedIconTheme: IconThemeData(size: 30),
  );

  static const dividerTheme = DividerThemeData(color: lines, thickness: 0.5);

  static ThemeData appTheme(BuildContext context) {
    _setTextTheme(context);
    return mainTheme = ThemeData(
      appBarTheme: appBarTheme,
      textTheme: mainFont,
      bottomNavigationBarTheme: bottomNavigationBarTheme,
      dividerTheme: dividerTheme,
      // iconTheme: const IconThemeData(color: secondary),
      colorScheme: ColorScheme.fromSeed(
        seedColor: primary,
        primary: primary,
        surface: lightBackground,
        primaryContainer: Colors.white,
        secondary: secondary,
        error: error,
        scrim: forGridOnly,
        onSurface: darkBackground,
      ),
    );
  }

  static ThemeData darkTheme(BuildContext context) {
    _setTextTheme(context);
    return mainTheme = ThemeData(
      appBarTheme: appBarTheme,

      textTheme: mainFont,
      bottomNavigationBarTheme: bottomNavigationBarTheme,
      dividerTheme: dividerTheme,

      // ColorScheme
      colorScheme: ColorScheme.fromSeed(
        brightness: Brightness.dark,
        seedColor: secondary,
        primary: primary,
        secondary: secondary,
        surface: darkBackground,
        primaryContainer: Colors.black,
        error: error,
        scrim: secondary,
        onSurface: darkTextBody,
      ),
    );
  }

  // We change the hights based on the design system
  static const hight8 = SizedBox(height: 8);
  static const hight16 = SizedBox(height: 16);
  static const hight24 = SizedBox(height: 24);
  static const hight32 = SizedBox(height: 32);
  static const hight40 = SizedBox(height: 40);
  static const hight48 = SizedBox(height: 48);
  static const hight56 = SizedBox(height: 56);
  static const hight64 = SizedBox(height: 64);
}
