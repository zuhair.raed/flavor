import 'package:auto_route/auto_route.dart';
import 'package:flavor/app/routes/app_router.gr.dart';

@AutoRouterConfig(replaceInRouteName: 'Screen,Route')
class AppRouter extends $AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: SplashRoute.page, initial: true),
        AutoRoute(page: MainRoute.page),
      ];
}
//cmd:  dart run build_runner build --delete-conflicting-outputs

