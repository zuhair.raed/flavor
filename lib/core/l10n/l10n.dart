import 'package:flutter/material.dart';

class L10n {
  static const supportedLocales = [
    Locale('ar'),
    Locale('en'),
  ];

  static String getFlag(String code) {
    switch (code) {
      case 'en':
        return '🇺🇸';
      case 'ar':
        return '🇸🇦';
      default:
        return '🌐';
    }
  }

  // language code enum
}
