import 'dart:convert';

// import 'package:crypto/crypto.dart' as crypto;
import 'package:intl/intl.dart';
import 'Log.dart';

extension StringExtension on String {
  int? toInt() {
    return int.tryParse(this);
  }

  double? toDouble() {
    return double.tryParse(this);
  }

  num? toNumber() {
    return num.tryParse(this);
  }

  // String htmlUnescape() {
  //   final unescape = HtmlUnescape();
  //   return unescape.convert(this);
  // }
}

class StringUtils {
  static bool isBlank(String text) {
    return !isNotBlank(text);
  }

  // static bool isHtml(String htmlText) {
  //   if (nullOrEmpty(htmlText).isEmpty) {
  //     return false;
  //   }
  //   // ignore: unnecessary_raw_strings
  //   final RegExp exp = RegExp(r"<[^>]*>", multiLine: true);

  //   return exp.hasMatch(htmlText);
  // }

  static bool isNotBlank(String text) {
    return text.trim().isNotEmpty;
  }

  static const List<String> trueString = ['true', 't', '1', 'on', 'yes', 'y'];
  static const List<String> falseString = [
    'false',
    'f',
    '0',
    'off',
    'no',
    'n',
  ];

  /// convertS string to bool
  static bool toBool(String str, {bool defaultValue = false}) {
    Log.e('toBool => $str');
    if (isBlank(str)) {
      return defaultValue;
    }
    final val = str.trim().toLowerCase();
    if (defaultValue) {
      for (final String s in falseString) {
        if (s == val) {
          return false;
        }
      }
      return true;
    } else {
      for (final String s in trueString) {
        if (s == str) {
          return true;
        }
      }
      return false;
    }
  }

  static String changeDigit(String innumber, NumStrLanguage toDigit) {
    final arabicNumbers = ['٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];

    final englishNumbers = [
      '0',
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '.',
    ];
    String number = innumber;
    if (toDigit == NumStrLanguage.english) {
      for (var i = 0; i < 10; i++) {
        Log.pr('relace all ${arabicNumbers[i]} with ${englishNumbers[i]}');
        //     .replaceAll(new RegExp(persianNumbers[i]), enNumbers[i])
        // ignore: parameter_assignments
        number = number.replaceAll(RegExp(arabicNumbers[i]), englishNumbers[i]);
      }
    } else {
      for (var i = 0; i < 10; i++) {
        number = number
            // .replaceAll(new RegExp(enNumbers[i]), persianNumbers[i])
            .replaceAll(RegExp(englishNumbers[i]), arabicNumbers[i]);
      }
    }
    return number;
  }

  // static String getMd5(String inp) {
  //   return crypto.sha1.convert(utf8.encode(inp)).toString();
  // }

  static String prettyJson(dynamic data, {int indent = 2}) {
    final spaces = ' ' * indent;
    // if (data is String) {
    //   try {
    //     // ignore: parameter_assignments
    //     data = json.decode(data);
    //   } on Exception catch (_) {}
    // }
    final encoder = JsonEncoder.withIndent(spaces);
    return encoder.convert(data);
  }

  static String formatnum(
    double? thenum, {
    String symbol = 'AED',
    int decimalDigits = 2,
  }) {
    final numval = NumberFormat.currency(
      locale: 'en_US',
      name: '',
      decimalDigits: decimalDigits,
    ).format(thenum);

    return '$numval $symbol';
  }

  static String getJson(Map<String, dynamic> data) {
    return json.encode(data);
  }

  static Map<String, dynamic>? asJson(String? data) {
    if (data == null) {
      return null;
    }
    return json.decode(data);
  }

  static bool isNumeric(String? s) {
    if (s == null) {
      return false;
    }

    return double.tryParse(s) != null;
  }
}

enum NumStrLanguage { faris, english }
